from django.contrib.auth.models import User, AbstractUser
from django.db import models


class CustomUser(AbstractUser):
    user_type = models.IntegerField(choices=((0, 'Admin'), (1, 'Participant')), default=0)
