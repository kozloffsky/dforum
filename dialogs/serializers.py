from rest_framework import serializers

from accounts.models import CustomUser
from dialogs.models import Message, Thread


class UserSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=True)
    user_type = serializers.IntegerField(default=1, required=False)

    class Meta:
        model = CustomUser
        fields = ('id', 'user_type')

    def get_object(self):
        return CustomUser.objects.get(pk=self.validated_data['id'])


class ThreadSerializer(serializers.ModelSerializer):
    participants = UserSerializer(read_only=True, many=True)

    class Meta:
        model = Thread
        fields = ('id', 'created', 'updated', 'participants')

    def create(self, validated_data):
        return Thread.create_new(validated_data.pop('author'))


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = ('id', 'thread', 'sender', 'text', 'created')
