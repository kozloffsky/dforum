from django.db import models

from accounts.models import CustomUser


class AuthorIsNotAdminException(Exception):
    pass


class Thread(models.Model):
    participants = models.ManyToManyField(CustomUser, related_name='threads')
    created = models.DateTimeField(auto_now=True, auto_created= True, editable=False)
    updated = models.DateTimeField(auto_now=True, auto_created=True, editable=False)

    @classmethod
    def create_new(cls, author):
        """
        Creates and saves new thread for given author. Threads can be only created by
        admin users
        :param author: CustomUser
        :return: Thread
        """
        if author.user_type is not 0:
            raise AuthorIsNotAdminException()
        thread = cls()
        thread.save()
        thread.participants.add(author)
        return thread

    def get_user_threads(self, user):
        return self.objects.filter(participants=user)


class Message(models.Model):
    thread = models.ForeignKey(Thread, related_name='messages', on_delete=models.CASCADE)
    sender = models.ForeignKey(CustomUser, related_name='messages', on_delete=models.CASCADE)
    text = models.TextField()
    created = models.DateTimeField(auto_now=True, auto_created=True, editable=False)
