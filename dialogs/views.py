from django.contrib.auth.models import AnonymousUser
from rest_framework import permissions
from rest_framework.decorators import action
from rest_framework.mixins import CreateModelMixin, ListModelMixin
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet
from rest_framework_extensions.mixins import NestedViewSetMixin

from accounts.models import CustomUser
from dialogs.models import Thread, Message
from dialogs.serializers import ThreadSerializer, MessageSerializer, UserSerializer


class IsAdmin(permissions.BasePermission):
    def has_permission(self, request, view):
        user = request.user
        print(user)
        if isinstance(user, AnonymousUser):
            return False
        return user.user_type is 0


class ThreadViewSet(NestedViewSetMixin, CreateModelMixin, ListModelMixin, GenericViewSet):
    permission_classes = (IsAdmin,)
    serializer_class = ThreadSerializer
    queryset = Thread.objects.all()

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)

    @action(methods=['post'], detail=True)
    def add_user(self, request, pk=None):
        thread = self.get_object()
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            try:
                user = serializer.get_object()
                if user.user_type is 0:
                    return Response("One admin allowed", status=405)
                thread.participants.add(user)
            except Exception as e:
                return Response(e, status=404)
            return Response("ok")
        return Response( "bad data", status=405)

    @action(methods=['post'], detail=True)
    def remove_user(self, request, pk=None):
        thread = self.get_object()
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            try:
                user = serializer.get_object()
                thread.participants.add(user)
                if user.user_type is 0 or len(thread.participants) is 0:
                    thread.delete()
            except Exception as e:
                return Response(e, status=404)
            return Response("ok")
        return Response( "bad data", status=405)


class MessageViewSet(NestedViewSetMixin, CreateModelMixin, ListModelMixin, GenericViewSet):
    serializer_class = MessageSerializer
    queryset = Message.objects.all()


class UserViewSet(GenericViewSet):
    queryset = CustomUser.objects.all()
