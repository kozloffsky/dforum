"""djangoforum URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from rest_framework.routers import DefaultRouter
from rest_framework_extensions.routers import ExtendedDefaultRouter
from rest_framework_jwt.views import obtain_jwt_token

from dialogs.views import ThreadViewSet, MessageViewSet, UserViewSet

router = ExtendedDefaultRouter()
router.register('threads', ThreadViewSet).register('messages',
                                                   MessageViewSet,
                                                   base_name='thread-message',
                                                   parents_query_lookups=['thread']
                                                   )
router.register('users', UserViewSet).register('threads', ThreadViewSet, base_name='user-thread', parents_query_lookups=['participants'])

urlpatterns = [
    path('admin/', admin.site.urls),
    #path('threads/', ThreadList.as_view()),
    path('api-token-auth/', obtain_jwt_token),
]

urlpatterns += router.urls
